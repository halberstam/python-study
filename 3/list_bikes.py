
motorcycles = ['honda', 'yamaha', 'suzuki']
print(motorcycles)
motorcycles[0] = 'ducati'
print(motorcycles)

motorcycles.append('hiro')
print(motorcycles)

motorcycles.insert(0, 'kawasaki')
print(motorcycles)

del motorcycles[0]
print(motorcycles)

popped_moto = motorcycles.pop()
print(popped_moto)

motorcycles = ['honda', 'yamaha', 'suzuki'] 
last_owned = motorcycles.pop() 
print("The last motorcycle I owned was a " + last_owned.title() + ".")
