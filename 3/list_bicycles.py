
bicycles = ['trek', 'cannondale', 'bmx', 'offroad']
print(bicycles[0].title())
print(bicycles[1].title())
print(bicycles[2].title())
print(bicycles[3].title())

# Print last bicycle
print(bicycles[-1].title())

message = "My first bicycle was a " + bicycles[0].title() + "."
print(message)
